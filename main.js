let mainPage = document.getElementsByClassName('welcome')[0];
mainPage.classList.add('active');
let learnPage = document.getElementsByClassName('learn')[0];
let trainPage = document.getElementsByClassName('train')[0];

let exercizeContainer = document.getElementsByClassName('learn-list')[0];
let trainContainer = document.getElementsByClassName('train-list')[0];



function Navigation(data) {
  this.arrow = document.getElementsByClassName(data.arrow)[0];
  this.navMenu = document.getElementsByClassName(data.navMenu)[0];
  this.learnMenu = document.getElementsByClassName(data.learnMenu)[0];
  this.trainMenu = document.getElementsByClassName(data.trainMenu)[0];
  this.numbersLearnList = this.learnMenu.children[0];
  this.numbersTrainList = this.trainMenu.children[0];

  this.openNavigation = function() {
    this.navMenu.classList.toggle('open-main-menu');
    this.arrow.classList.toggle('arrows-rotate');
  }.bind(this);

  this.optionMenuOpen = function(event) {
    if(event.target.className === 'learn-menu'){
      this.numbersLearnList.classList.toggle('reveal-numbers');
    } else if (event.target.className === 'train-menu') {
      this.numbersTrainList.classList.toggle('reveal-numbers');
    }
  }.bind(this);

  this.closeNavigation = function(event) {
    this.navMenu.classList.remove('open-main-menu');
    this.arrow.classList.remove('arrows-rotate');
    if(event.target.tagName === 'LI' && event.target.parentNode.className === 'learn-ul reveal-numbers') {
      this.numbersLearnList.classList.toggle('reveal-numbers');
      mainPage.classList.remove('active');
      learnPage.classList.add('active');
      trainPage.classList.remove('active');
    } else if(event.target.tagName === 'LI' && event.target.parentNode.className === 'train-ul reveal-numbers') {
      this.numbersTrainList.classList.toggle('reveal-numbers');
      mainPage.classList.remove('active');
      trainPage.classList.add('active');
      learnPage.classList.remove('active');
    }
  }.bind(this);

  this.init = function() {
    this.arrow.addEventListener('click', this.openNavigation);
    this.learnMenu.addEventListener('click', this.optionMenuOpen);
    this.trainMenu.addEventListener('click', this.optionMenuOpen);
    this.numbersLearnList.addEventListener('click', this.closeNavigation);
    this.numbersTrainList.addEventListener('click', this.closeNavigation);
  }.bind(this);

  this.init();
};

let navigation = new Navigation ({
  arrow: 'fa-angle-down',
  navMenu: 'menu',
  learnMenu: 'learn-menu',
  trainMenu: 'train-menu',
});


function LearnNumbers(RenderContainerClass) {
  this.container = document.getElementsByClassName(RenderContainerClass)[0];

  this.getNumberLearn = function(event) {
    if(event.target.tagName==='LI' && event.target.parentNode.className==='learn-ul') {
      this.learnNumber = +event.target.innerHTML;
      this.container.innerHTML = '';
      for (var i = 1; i < 10; i++) {
        this.container.innerHTML += `<p>${i} * ${this.learnNumber} = <span>${i*this.learnNumber}</span>;</p>`
      }
    }
  }.bind(this);

  document.body.addEventListener('click', this.getNumberLearn);
}

let learnNumbers = new LearnNumbers('learn-list');



function TrainNumbers(RenderContainerClass) {
    this.container = document.getElementsByClassName(RenderContainerClass)[0];
    this.getNumberTrain = function(event) {
      if(event.target.tagName==='LI' && event.target.parentNode.className==='train-ul') {
        this.trainNumber = +event.target.innerHTML;
        this.container.innerHTML = '';
        for (var i = 1; i < 7; i++) {
          var randomNumber = Math.floor(1+Math.random()*9);
          var result = randomNumber*this.trainNumber;
          this.container.innerHTML += `<p>${randomNumber} * ${this.trainNumber} = <i>${result}</i><input type = "text"><button type = "button">ОК</button>;</p>`
        }
      }
    }.bind(this);

    this.check = function(event) {
        if(event.target.tagName === 'BUTTON' || event.target.tagName === 'button') {
          if (+event.target.previousElementSibling.value == +event.target.previousElementSibling.previousElementSibling.innerHTML) {
            event.target.previousElementSibling.classList.add('true');
          } else {
            event.target.previousElementSibling.classList.add('false');
          }
          event.target.classList.toggle('hide');
        }
    }.bind(this);

  
    document.body.addEventListener('click', this.getNumberTrain);
    document.body.addEventListener('click', this.check);
}

let trainNumbers = new TrainNumbers('train-list');